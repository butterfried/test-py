## decalre const
CONFIG_FILE_NAME     = 'config.ini'
SYSTEM_CONFIG        = 'SYSTEM'
SENSOR_CONFIG        = 'SENSOR'
DISPLAY_CONFIG       = 'DISPLAY'
DATA_SENDER_CONFIG   = 'DATA_SENDER'
SENSOR_PREFIX_CONFIC = 'SENSOR_'

MODE_DEV  = 'dev'
MODE_PROD = 'prod'

## config
config = None

config_system      = None
config_system_mode = None

config_sensor           = None
config_sensor_amount    = None
config_sensor_tick_time = None

config_display                      = None
config_display_tick_time            = None
config_display_change_every_n_ticks = None

config_data_sender           = None
config_data_sender_tick_time = None
config_data_sender_url       = None
config_data_sender_api_key   = None

## declare global variable
sensors_data_dict   = None
display             = None