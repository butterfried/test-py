import RPi.GPIO as GPIO
import time
import threading
import asyncio
import requests
import json
import logging
import lib.lcddriver as lcddriver
import system_var as sys
from configparser import ConfigParser
from aiohttp import web
import src.threads.data_sender as data_sender
import src.threads.lcd_display as lcd_display
import src.threads.gpio_reader as gpio_reader
import src.threads.gpio_reader_dummy as gpio_reader_dummy
import src.classes.sensor as sensor

## Creates a new Aiohttp Web Application
web_app = web.Application()
## router
# async def index(request):
#     ## define web server function
#     with open('index.html') as f:
#         return web.Response(text=f.read(), content_type='text/html')
# web_app.router.add_get('/', index)

async def reset(request):
    sys.display.lcd_clear()
    for key in sys.sensors_data_dict:
            sys.sensors_data_dict[key].daily_value = 0
    res = {"result": "success"}
    return web.json_response(res)
web_app.router.add_get('/reset', reset)

def load_config():
    sys.config = ConfigParser()
    sys.config.read(sys.CONFIG_FILE_NAME)

    # SYSTEM CONFIG
    sys.config_system       = sys.config[sys.SYSTEM_CONFIG]
    sys.config_system_mode  = sys.config_system['mode']

    # SENSOR CONFIG
    sys.config_sensor           = sys.config[sys.SENSOR_CONFIG]
    sys.config_sensor_amount    = int(sys.config_sensor['amount'])
    sys.config_sensor_tick_time = float(sys.config_sensor['tick_time'])

    # LED DISPLAY CONFIG
    sys.config_display                      = sys.config[sys.DISPLAY_CONFIG]
    sys.config_display_tick_time            = float(sys.config_display['tick_time'])
    sys.config_display_change_every_n_ticks = float(sys.config_display['change_every_n_ticks'])

    # DATA SENDER CONFIG
    sys.config_data_sender           = sys.config[sys.DATA_SENDER_CONFIG]
    sys.config_data_sender_tick_time = int(sys.config_data_sender['tick_time'])
    sys.config_data_sender_url       = sys.config_data_sender['url']
    sys.config_data_sender_api_key   = sys.config_data_sender['api_key']


def init():
    setup_gpio_pin()
    prepare_sensor()

def setup_gpio_pin():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

def prepare_sensor():
    sys.sensors_data_dict = {}
    for sensor_index in range(sys.config_sensor_amount):
        # get config of [SENSOR_X] for initial object sensor
        pin_config_name = sys.SENSOR_PREFIX_CONFIC + str(sensor_index)
        pin_config = sys.config[pin_config_name]

        # prepare init data for each sensor
        sensor_id = pin_config['id']
        name      = pin_config['name']
        gpio_pin  = int(pin_config['gpio_pin'])

        # register sensor object
        sys.sensors_data_dict[ pin_config_name ] = sensor.Sensor(sensor_id, name, gpio_pin)

        # set GPIO pin mode
        GPIO.setup( gpio_pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN )

if __name__ == '__main__':
    ## init program & load config
    load_config()
    init()
    ## start thread

    if sys.config_system_mode == sys.MODE_PROD:
        # read input
        readGPIOInputThread = gpio_reader.ReadGPIOInputThread()
    elif sys.config_system_mode == sys.MODE_DEV:
        # generate input (dummy for real sensor)
        readGPIOInputThread = gpio_reader_dummy.GenerateGPIOInputThread()
    else:
        print('<MODE> is not set')
        exit()

    # display daily counter on LCD display
    displaySensorDataThread = lcd_display.DisplaySensorDataThread()
    # send counter data every 5 seconds
    sendSensorsDataHttpThread = data_sender.SendSensorsDataHttpThread()
    ## start web server
    web.run_app(web_app)