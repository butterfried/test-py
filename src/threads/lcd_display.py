import time
import threading
import asyncio
import system_var as sys
import lib.lcddriver as lcddriver
import logging

## Thread for displaying daily sensor data
class DisplaySensorDataThread(object):
    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
    def run(self):
        setup_display()
        asyncio.run(lcd_display_sensor_data())

def setup_display():
    ## Load the driver and set it to "display"
    ## If you use something from the driver library use the "display." prefix first
    sys.display = lcddriver.lcd()

def lcd_display_sensor_data():
    display_tick_count   = 0
    current_sensor_index = 0
    last_sensor_index = None
    last_value_display = None
    while True:
        sensor_data = sys.sensors_data_dict[sys.SENSOR_PREFIX_CONFIC + str(current_sensor_index)]
        
        try:
            # 1st line: check sensor if sensor is changed then re-render
            if current_sensor_index != last_sensor_index:
                sys.display.lcd_clear()
                sys.display.lcd_display_string(sensor_data.name, 1)
                last_sensor_index = current_sensor_index

            # 2nd line: check char lengh, if shorter then clear the display
            display_value = str(sensor_data.daily_value)
            if last_value_display is None or len(display_value) < len(last_value_display):
                last_value_display = display_value
                sys.display.lcd_clear()
            sys.display.lcd_display_string(str(sensor_data.daily_value), 2)

            display_tick_count += 1
            if display_tick_count >= sys.config_display_change_every_n_ticks:
                # reset tick count
                display_tick_count   = 0
                # start over
                current_sensor_index = (current_sensor_index + 1) % sys.config_sensor_amount
        except Exception as e:
            logging.warning(str(e))

        time.sleep(sys.config_display_tick_time)