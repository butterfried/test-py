import time
import threading
import asyncio
import system_var as sys
import RPi.GPIO as GPIO
import random

class GenerateGPIOInputThread(object):
    ## Thread for reading data from gpio input
    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
    def run(self):
        asyncio.run(background_task())

async def background_task():
    gacha = {
        'SENSOR_0': 1,
        'SENSOR_1': 2,
        'SENSOR_2': 3,
        'SENSOR_3': 4,
        'SENSOR_4': 5
    }
    while True:
        for key in sys.sensors_data_dict:
            dummy_signal = random.randint(1,100) <= gacha[key]
            if dummy_signal:
                if sys.sensors_data_dict[key].is_found:
                    sys.sensors_data_dict[key].value = sys.sensors_data_dict[key].value + 1
                    sys.sensors_data_dict[key].daily_value = sys.sensors_data_dict[key].daily_value + 1
                    sys.sensors_data_dict[key].is_found = False
            else:
                if not sys.sensors_data_dict[key].is_found:
                    sys.sensors_data_dict[key].is_found = True
        time.sleep(sys.config_sensor_tick_time)