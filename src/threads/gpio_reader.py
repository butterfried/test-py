import time
import threading
import asyncio
import system_var as sys
import RPi.GPIO as GPIO
import logging

class ReadGPIOInputThread(object):
    ## Thread for reading data from gpio input
    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
    def run(self):
        asyncio.run(background_task())

async def background_task():
    while True:
        try:
            for key in sys.sensors_data_dict:
                if GPIO.input(sys.sensors_data_dict[key].gpio_pin) == GPIO.HIGH:
                    if sys.sensors_data_dict[key].is_found:
                        sys.sensors_data_dict[key].value = sys.sensors_data_dict[key].value + 1
                        sys.sensors_data_dict[key].daily_value = sys.sensors_data_dict[key].daily_value + 1
                        sys.sensors_data_dict[key].is_found = False
                else:
                    if not sys.sensors_data_dict[key].is_found:
                        sys.sensors_data_dict[key].is_found = True
        except Exception as e:
            logging.warning(str(e))
        time.sleep(sys.config_sensor_tick_time)