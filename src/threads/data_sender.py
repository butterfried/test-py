import time
import threading
import asyncio
import system_var as sys
import logging
import requests
import json

class SendSensorsDataHttpThread(object):
    ## Thread for sending counter data to main server every 5 seconds
    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
    def run(self):
        asyncio.run(send_sensor_data_http())

def dumper(obj):
    try:
        return obj.toJSON()
    except Exception:
        return obj.__dict__

def send_sensor_data_http():
    while True:
        # http request
        post_data = {
            "key": sys.config_data_sender_api_key,
            "data": json.loads(json.dumps(sys.sensors_data_dict, default=dumper, indent=2))
        }
        # reset
        try:
            for key in sys.sensors_data_dict:
                sys.sensors_data_dict[key].value = 0
                r = requests.post(sys.config_data_sender_url, json = post_data)
            # print(r.text)
        except Exception as e:
            logging.warning(str(e))
        time.sleep(sys.config_data_sender_tick_time)