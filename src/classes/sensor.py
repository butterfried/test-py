class Sensor:
    def __init__(self, id, name, gpio_pin):
        self.id          = id
        self.name        = name
        self.gpio_pin    = gpio_pin
        self.is_found    = False
        self.daily_value = 0
        self.value       = 0
        self.validator   = 0